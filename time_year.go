package utils

import (
	"time"
	//"fmt"
)


func LstNYear(rq string, N int) (ret string) {

	var ntm time.Time
	tm, err := time.Parse("20060102", rq)

	if err != nil {
		return rq
	} else {
	
		if( N > 0 ) {
			N = 0 - N
		}
	
		ntm = tm.AddDate(N, 0, 0)
		ret = ntm.Format("20060102")
	}

	return ret	
}


func Lst3Year(rq string) (ret string) {
	return LstNYear(rq, 3)
}


func Lst2Year(rq string) (ret string) {
	return LstNYear(rq, 2)
}

func Lst1Year(rq string) (ret string) {
	return LstNYear(rq, 1)
}