package utils

import (
	"fmt"
	"strconv"
)

func Round2(value float64) float64 {
	value, _ = strconv.ParseFloat(fmt.Sprintf("%.2f", value), 64)
	return value
}

func Round3(value float64) float64 {
	value, _ = strconv.ParseFloat(fmt.Sprintf("%.3f", value), 64)
	return value
}

func Round5(value float64) float64 {
    value, _ = strconv.ParseFloat(fmt.Sprintf("%.5f", value), 64)
    return value
}