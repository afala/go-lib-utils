package utils

import (
	"time"
)

func LstQuarter(rq string) (ret string) {
	var ntm time.Time
	tm, err := time.Parse("20060102", rq)

	if err != nil {
		return rq
	} else {
		ntm = tm.AddDate(0, -3, 0)
		/*
		if ntm.Month() == time.October {
			ntm = ntm.AddDate(0, 0, -1)
		}
		*/
		ret = ntm.Format("20060102")
	}

	return ret
}


func NxtQuarter(rq string) (ret string) {
	var ntm time.Time
	tm, err := time.Parse("20060102", rq)

	if err != nil {
		return rq
	} else {
		ntm = tm.AddDate(0, 3, 0)
		ret = ntm.Format("20060102")
	}

	return ret
}