package utils

import (
	"strconv"
)

func StringToFloat(s string) (d float64) {
	d, err := strconv.ParseFloat(s, 64)

	if err == nil {
		return d
	} else {
		return 0
	}
}

func StringToUint32(s string) (d uint32) {
	d1, err := strconv.ParseUint(s, 10, 32)
	if err == nil {
		return uint32(d1)
	} else {
		return 0
	}
}

func StringToUint64(s string) (d uint64) {
	d1, err := strconv.ParseUint(s, 10, 64)
	if err == nil {
		return d1
	} else {
		return 0
	}
}

func Float64ToString(s float64) (d string) {
	d = strconv.FormatFloat(s, 'E', -1, 64)
	return d
}

func Uint32ToString(s uint32) (d string) {
	d = strconv.FormatUint(uint64(s), 10)
	return d
}


func Uint64ToString(s uint64) (d string) {
	d = strconv.FormatUint(s, 10)
	return d
}
